import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

class Storage {
    //storage
    private String seq[];

    //MB need constructor
    public void read(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(new File(fileName)));

        String line = null;
        while ((line = br.readLine()) != null) {
            seq = line.split(" ");
            return;
        }
        br.close();

    }
    public void print() {
        for (int i = 0; i < seq.length; i++) {
            //if (i != 0) System.out.print(" ");
            System.out.print(seq[i] + " ");
        }
        System.out.println();
    }

    public void printDigits() {
        //boolean firstFoind = false;
        for (int i = 0; i < seq.length; i++)
            if (seq[i].compareTo("A") >= 0 && seq[i].compareTo("Z") <= 0) {
                System.out.print(" ");
                System.out.println(seq[i] + " ");
                //firstFoind = true;
            }
        System.out.println();
    }

    public void printDigitsWODoubles() {
        String[] s = seq;
        String prev = "no_char";
        boolean firstFound = false;
        for (int i = 0; i < s.length; i++)
            if (s[i].compareTo("A") >= 0 && s[i].compareTo("Z") <= 0 && !s[i].equals(prev)) {
                prev = s[i];
                if (firstFound) System.out.print(" ");
                System.out.print(s[i]+" ");
                firstFound = true;
            }
        System.out.println();
    }

    public void sort() {
        String[] s = seq;
        //sort
        //Array.sort(s);
        //
        for (int i = 0; i < s.length; i++)
            for (int j = 0; j < s.length; j++)
                if (s[i].compareTo(s[j]) > 0) {
                    String t = s[i];
                    s[i] = s[j];
                    s[j] = t;
                }
    }
}
