import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Task0 {
    //solution task 0.1
    public static void main(String[] args) {

        //read data
        Storage st = new Storage();
        try {
            st.read(args[0]);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        // print source data
        st.print();

        //print digits
        st.printDigits();

        // sort and print digits
        st.sort();
        st.printDigits();

        //print sorted digits by once
        st.printDigitsWODoubles();

    }
}

